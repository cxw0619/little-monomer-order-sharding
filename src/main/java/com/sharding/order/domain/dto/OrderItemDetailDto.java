package com.sharding.order.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ruyuan
 * 订单明细dto
 */
@Data
public class OrderItemDetailDto implements Serializable {
    /**
     * 主键id
     */
    private Long id;

    /**
     *订单号
     */
    private String orderNo;

    /**
     *商品ID
     */
    private Long productId;

    /**
     * 商品图片
     */
    private String productPictureUrl;

    /**
     *商品名称
     */
    private String productName;

    /**
     *商品分类ID
     */
    private Long categoryId;

    /**
     *商品购买数量
     */
    private Integer goodsNum;

    /**
     *商品单价
     */
    private BigDecimal goodsPrice;

    /**
     *商品总价
     */
    private BigDecimal goodsAmount;

    /**
     *商品优惠金额
     */
    private BigDecimal discountAmount;

    /**
     *参与活动ID
     */
    private Long discountId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建用户
     */
    private Long createUser;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 更新用户
     */
    private Long updateUser;

    /**
     * 是否已删除
     */
    private Integer deleteFlag;
}
