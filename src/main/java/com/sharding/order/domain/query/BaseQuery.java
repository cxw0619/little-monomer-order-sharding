package com.sharding.order.domain.query;

import lombok.Data;

/**
 *
 * @author ruyuan
 * 查询请求入参基类
 */
@Data
public class BaseQuery{

    /**
     * 页数
     */
    private Integer pageNo = 1;

    /**
     * 每页大小
     */
    private Integer pageSize = 10;

}
