package com.sharding.order.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ruyuan
 * @Description
 */
@Data
public class BaseQueryVO implements Serializable {

    /**
     * 逻辑删除标识
     */
    private Integer deleteFlag;

}
