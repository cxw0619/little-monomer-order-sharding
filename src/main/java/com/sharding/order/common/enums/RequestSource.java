package com.sharding.order.common.enums;

/**
 * 请求来源
 */
public enum RequestSource {
    C(10, "C端请求"),
    B(20, "B端请求");

    public final Integer code;
    public final String desc;

    RequestSource(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
