package com.sharding.order.common.util;

import java.util.Calendar;
import java.util.Date;

/**
 * @author ruyuan
 * 日期工具类
 */
public class DateUtil {

    /**
     * 比较日期是否在当前日期之前
     * @param target
     * @return
     */
    public static boolean before(Date target) {
        return target.before(new Date());
    }

    /**
     * 比较日期是否在当前日期之后
     * @return
     */
    public static boolean after(Date target) {
        return !before(target);
    }

    /**
     * 获取当前日期加上一个时间范围后的时间
     * @param number
     * @return
     */
    public static Date addMinutes(int number) {
        return add(Calendar.MINUTE, number);
    }

    /**
     * 获取当前日期加上一个时间范围后的时间
     * @param unit
     * @param number
     * @return
     */
    public static Date add(int unit, int number) {
        return add(Calendar.getInstance().getTime(), unit, number);
    }

    /**
     * 获取某个日期加上一个时间范围的时间
     * @param left
     * @param unit
     * @param number
     * @return
     */
    public static Date add(Date left, int unit, int number) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(left);
        instance.add(unit, number);
        return instance.getTime();
    }

}
